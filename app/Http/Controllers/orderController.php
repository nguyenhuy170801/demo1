<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\order;
use App\Models\orderDetail;
use App\Models\province;
use App\Models\product;

class orderController extends Controller
{
    
    public function listOrderByDate($date)
    {
        $province = province::all();
        if($date!=''){
        $listOrder = order::whereDate('created_at',$date)->get();
        // $listOrderDetail=$listOrder->orderDetails;
        // dd($listOrder)
        // foreach( $listOrder as $value){
        //     dd($value->orderDetails);
        // }
        // dd($array);
        // dd($listOrder);
        $listprovince3 = $listOrder->where('area_id',3)->first();
       
        $listprovince2 = $listOrder->where('area_id',2)->first();
        $listprovince1 = $listOrder->where('area_id',1);
        // dd($listOrder);
        // foreach($listprovince3 as $value){
        //    $arrayProvince3[] = $value->province_id;
        // }
        // $arrayProvince3 = array_unique($arrayProvince3);
        // dd($listprovince3);
        // $arrayProvince3 = asort($arrayProvince3);
        // dd($listprovince3);
        // dd($arrayProvince3);
        // $product = product::all();
        $product= product::orderBy('sort','asc')->get();
        // $listOrderBydate=$product->orderDetails;
        // dd($listOrderBydate);
        }
        return view('view', compact('listOrder'))->with(['province'=>$province,'product'=>$product,'listprovince3'=>$listprovince3,'listprovince2'=>$listprovince2,'date'=>$date]);
    }

    public function listOrder()
    {
        $province = province::all();
       
        $listOrder = order::get();
        // dd($listOrder);
        // dd($listOrder);
        $listprovince3 = $listOrder->where('area_id',3)->first();
       
        $listprovince2 = $listOrder->where('area_id',2)->first();
        $listprovince1 = $listOrder->where('area_id',1);
        // dd($listprovince2);
        // foreach($listprovince3 as $value){
        //    $arrayProvince3[] = $value->province_id;
        // }
        // $arrayProvince3 = array_unique($arrayProvince3);
        // dd($listprovince3);
        // $arrayProvince3 = asort($arrayProvince3);
        // dd($listprovince3);
        // dd($arrayProvince3);
        // $product = product::all();
        $product= product::orderBy('sort','asc')->get();
        $date='';
        
        return view('view', compact('listOrder'))->with(['province'=>$province,'product'=>$product,'listprovince3'=>$listprovince3,'date'=>$date,'listprovince2'=>$listprovince2]);
    }
}
