<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    use HasFactory;
    protected $table = 'v2_orders';
    public function orderDetails()
    {
        return $this->hasMany(orderDetail::class,'v2_order_id','id');
    }
}
