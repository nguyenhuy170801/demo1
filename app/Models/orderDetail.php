<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class orderDetail extends Model
{
    use HasFactory;
    protected $table = 'v2_order_detail';
    public function order()
    {
        return $this->belongsTo(order::class,'v2_order_id');
    }
}
