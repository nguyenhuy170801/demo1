<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    use HasFactory;
    protected $table = 'products';
    public function orderDetails()
    {
        return $this->hasMany(orderDetail::class,'product_id','id');
    }
}
