<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>demo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h2>ĐƠN HÀNG Ở MIỀN NAM</h2>
        <p>Thứ 2 ngày 17/06/2023</p>
        {{-- <p>Miền Nam</p> --}}


        @if ($date=='')
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th>STT</th>
              <th>Sản Phẩm/Tỉnh</th>
             @foreach ( $province as $value )
             {{-- @dd($listprovince3) --}}
              @if($listprovince3->col1_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince3->col2_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince3->col3_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince3->col4_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince3->col5_province_id==$value->id)
              <th>{{ $value->name }}</th>
              @endif
              @if($listprovince3->col6_province_id==$value->id)
              <th>{{ $value->name }}</th>
              @endif
            
             @endforeach
            </tr>
          </thead>
          <tbody>
         
            @foreach ($product as $value )
            
            <tr>
              <td>{{  $value->sort }}</td>
              <td>{{  $value->name }}</td>
              {{-- @dd($value->orderDetails) --}}
              <td>
                @foreach ($value->orderDetails as $value2 )
                {{-- @dd($value2) --}}
                {{-- @dd($value2->order) --}}
                @if ($value2->order->col1_province_id==1 && $value2->order->area_id==3)
                <p>{{ $value2->col1_value}}</p>
                @else
                <p>0</p>
                @endif
                @endforeach
              </td>
                
              <td>
                @foreach ($value->orderDetails as $value2 )
                @if ($value2->order->col2_province_id==2 && $value2->order->area_id==3)
                <p>{{ $value2->col2_value}}</p>
                @else
                <p>0</p>
                @endif
                @endforeach
              </td>

              <td>
                @foreach ($value->orderDetails as $value2 )
                @if ($value2->order->col3_province_id==3 && $value2->order->area_id==3)
                <p>{{ $value2->col3_value}}</p>
                @else
                <p>0</p>
                @endif
                @endforeach
              </td>

              

            </tr>
            @endforeach
            
          </tbody>
        </table>
        @endif

        @if ($date!='')
        @if($listprovince3!=null)   
        
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th>STT</th>
              <th>Sản Phẩm/Tỉnh</th>
             @foreach ( $province as $value )
             {{-- @dd($listprovince3) --}}
              @if($listprovince3->col1_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince3->col2_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince3->col3_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince3->col4_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince3->col5_province_id==$value->id)
              <th>{{ $value->name }}</th>
              @endif
              @if($listprovince3->col6_province_id==$value->id)
              <th>{{ $value->name }}</th>
              @endif
            
             @endforeach
            </tr>
          </thead>
          <tbody>
         
            @foreach ($product as $value )
             
            <tr>
              
              <td>{{  $value->sort }}</td>
              <td>{{  $value->name }}</td>
            
              <td>
                @foreach ($listOrder as $list)
                {{-- @dd($list) --}}
                @foreach ($list->orderDetails as $value2 )
                {{-- @dd($value2) --}}
                {{-- @dd($value2->order) --}}
                {{-- @dd($value->id) --}}
                {{-- @dd($value2->product_id) --}}
                @if($value->id==$value2->product_id)
                    @if ($value2->order->col1_province_id==1 && $value2->order->area_id==3)
                    <p>{{ $value2->col1_value}}</p>
                    {{-- @else
                    <p>0</p> --}}
                    @endif
                    @endif
                    @endforeach
                    @endforeach
                    
                  </td>
                    
                  <td>
                    @foreach ($listOrder as $list)
                    @foreach ($list->orderDetails as $value2 )
                    {{-- @dd($value2) --}}
                    {{-- @dd($value2->order) --}}
                    @if($value->id==$value2->product_id)
                    @if ($value2->order->col2_province_id==2 && $value2->order->area_id==3)                
                    <p>{{ $value2->col2_value}}</p>
                    {{-- @else
                    <p>0</p> --}}
                  @endif
                    @endif
                    @endforeach

                    @endforeach
                    
                  </td>

                  <td>
                    @foreach ($listOrder as $list)

                    @foreach ($list->orderDetails as $value2 )
                    {{-- @dd($value2) --}}
                    {{-- @dd($value2->order) --}}
                    @if($value->id==$value2->product_id)
                    @if ($value2->order->col3_province_id==3 && $value2->order->area_id==3)                
                    <p>{{ $value2->col3_value}}</p>
                    {{-- @else
                    <p>0</p> --}}
                      @endif
                      @endif
                    
                @endforeach

                @endforeach
                
              </td>

              

            </tr>
            @endforeach
            
          </tbody>
        </table>
        @else
        <h2>Miền Nam Không có đơn</h2>
        @endif
        @endif
        ========================================================================

        {{-- Miền Trung --}}
        <h2>ĐƠN HÀNG Ở MIỀN TRUNG</h2>
        <p>Thứ 2 ngày 17/06/2023</p>
        @if ($date=='')
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th>STT</th>
              <th>Sản Phẩm/Tỉnh</th>
             @foreach ( $province as $value )
             {{-- @dd($listprovince3) --}}
              @if($listprovince2->col1_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince2->col2_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince2->col3_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince2->col4_province_id==$value->id)
                <th>{{ $value->name }}</th>
              @endif
              @if($listprovince2->col5_province_id==$value->id)
              <th>{{ $value->name }}</th>
              @endif
              @if($listprovince2->col6_province_id==$value->id)
              <th>{{ $value->name }}</th>
              @endif
            
             @endforeach
            </tr>
          </thead>
          <tbody>
         
            @foreach ($product as $value )
            
            <tr>
              <td>{{  $value->sort }}</td>
              <td>{{  $value->name }}</td>
              {{-- @dd($value->orderDetails) --}}
              <td>
                @foreach ($value->orderDetails as $value2 )
                {{-- @dd($value2) --}}
                {{-- @dd($value2->order) --}}
                @if ($value2->order->col1_province_id==4 && $value2->order->area_id==2)
                <p>{{ $value2->col1_value}}</p>
                @else
                <p>0</p>
                @endif
                @endforeach
              </td>
                
              <td>
                @foreach ($value->orderDetails as $value2 )
                @if ($value2->order->col2_province_id==5 && $value2->order->area_id==2)
                <p>{{ $value2->col2_value}}</p>
                @else
                <p>0</p>
                @endif
                @endforeach
              </td>

              

              

            </tr>
            @endforeach
            
          </tbody>
        </table>
        @endif

        @if ($date!='')
        @if($listprovince2!=null)   

        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th>STT</th>
              <th>Sản Phẩm/Tỉnh</th>
              @foreach ( $province as $value )
              {{-- @dd($listprovince2) --}}
               @if($listprovince2->col1_province_id==$value->id)
                 <th>{{ $value->name }}</th>
               @endif
               @if($listprovince2->col2_province_id==$value->id)
                 <th>{{ $value->name }}</th>
               @endif
               @if($listprovince2->col3_province_id==$value->id)
                 <th>{{ $value->name }}</th>
               @endif
               @if($listprovince2->col4_province_id==$value->id)
                 <th>{{ $value->name }}</th>
               @endif
               @if($listprovince2->col5_province_id==$value->id)
               <th>{{ $value->name }}</th>
               @endif
               @if($listprovince2->col6_province_id==$value->id)
               <th>{{ $value->name }}</th>
               @endif
             
              @endforeach
            </tr>
          </thead>
          <tbody>
         
            @foreach ($product as $value )
             
            <tr>
              
              <td>{{  $value->sort }}</td>
              <td>{{  $value->name }}</td>
            
              <td>
                @foreach ($listOrder as $list)
                {{-- @dd($list) --}}
                @foreach ($list->orderDetails as $value2 )
                {{-- @dd($value2) --}}
                {{-- @dd($value2->order) --}}
                {{-- @dd($value->id) --}}
                {{-- @dd($value2->product_id) --}}
                @if($value->id==$value2->product_id)
                    @if ($value2->order->col1_province_id==4 && $value2->order->area_id==2)
                    <p>{{ $value2->col1_value}}</p>
                    @else
                    <p>0</p>
                    @endif
                    @endif
                    @endforeach
                    @endforeach
                    
                  </td>
                    
                  <td>
                    @foreach ($listOrder as $list)
                    @foreach ($list->orderDetails as $value2 )
                    {{-- @dd($value2) --}}
                    {{-- @dd($value2->order) --}}
                    @if($value->id==$value2->product_id)
                    @if ($value2->order->col2_province_id==5 && $value2->order->area_id==2)                
                    <p>{{ $value2->col2_value}}</p>
                    @else
                    <p>0</p>
                  @endif
                    @endif
                    @endforeach

                    @endforeach
                    
                  </td>

                  
              
              

            </tr>
            @endforeach
            
          </tbody>
        </table>
        @else
        <h2>Miền Trung Không Có Đơn</h2>
        @endif
        
        @endif



    </div>
</body>
</html>